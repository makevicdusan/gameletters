#**Game in react**

Test for the game developer position.

#**Download**

```
cd /path_to_file_where_you_want_to_clone
```
```
git clone https://makevicdusan@bitbucket.org/makevicdusan/gameletters.git
```

#**Installation**

```
cd gameletters
```
```
npm install
```

#**Run application**

```
npm start
```
