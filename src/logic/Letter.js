import { MyError } from '../errors/MyError.js';

export class Letter {
  constructor(frame, x, y, width, height, letter) {
    this.letter = letter;
    this.frame = frame;
    this.previousX = x;
    this.previousY = y;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.ctx = frame.getContext('2d');
    this.color = 'rgb(122,122,122)';
    this.angle = 0;
    this.previousAngle = 0;
    this.interval = null;
    this.speedX = 0;
    this.speedY = 0;
    this.rotationSpeed = 0;
    this.rotationOff = false;
    this.isOnPlace = false;
    this.time = 0;
    this.letters = null;
  }

  setRotationSpeed(angle) {
    this.rotationSpeed = (Math.PI * angle) / 180;
  }
  setAngle(angle) {
    this.angle = angle * Math.PI/180;
  }
  getAngle() {
    return (this.angle * 180) /  Math.PI;
  }
  setPreviousAngle() {
    this.previousAngle = this.angle;
  }
  rotationDirection() {
    if(this.angle > this.previousAngle){
      return 'right';
    }else if(this.angle < this.previousAngle){
      return 'left';
    }
    return null;
  }
  rotate(){
    if(this.rotationSpeed === 0){
      return;
    }
      let angle = this.angle;
      angle += this.rotationSpeed;
      this.angle = angle % (2 * Math.PI);
  }

  stop() {
    clearInterval(this.interval);
  }

  update() {
    this.interval = setInterval(() => {
      this.letters.forEach((l) => {
        if(l !== this){
          this.touched(l);
        }
      });
      this.move();
      this.setPreviousPosition();
      this.rotate();
      this.setPreviousAngle();
      this.isOutsideFrame();
    },1);
  }

  calculateFontSize(){
    if(window.innerWidth > 1024){
      return 70;
    }else if(window.innerWidth > 600) {
      return (0.00011 * (this.frame.offsetWidth * this.frame.offsetHeight));
    }
    return (0.00017 * (this.frame.offsetWidth * this.frame.offsetHeight));
  }
  draw() {
      this.ctx.save();
      this.ctx.translate(this.x + this.width/2,this.y+this.height/2);
      this.ctx.rotate(this.angle);
      this.ctx.fillStyle = this.color;
      this.ctx.fillRect(-this.width/2,-this.height/2,this.width,this.height);
      let fontSize = this.calculateFontSize();
      this.ctx.font = `${fontSize}px Arial`;
      this.ctx.fillStyle = "rgb(0,89,0)";
      this.ctx.textAlign = "center";
      this.ctx.fillText(this.letter,0,this.height/2);
      this.ctx.restore();
  }
  setPosition(x, y) {
      this.x = x;
      this.y = y;
  }
  isUpsideDown() {
    if((this.y) < this.y){

    }
  }
  calculateSpeed() {
    return Math.sqrt(Math.pow(this.speedX, 2) + Math.pow(this.speedY, 2));
  }
  calculateDistance(fromX, fromY, x,y) {
    let xAps = Math.abs(x - fromX);
    let yAps = Math.abs(y - fromY);
    return Math.sqrt(Math.pow(xAps,2) + Math.pow(yAps, 2));
  }

  setSpeedToDot(speedX,speedY,x,y) {
    let distanceX = x - this.x;
    let distanceY = y - (this.y + this.height);
    if(!speedX){
      this.speedY = Number(speedY);
      this.speedX = Number((distanceX / distanceY) * speedY);
    }
    if(!speedY){
      this.speedX = Number(speedX);
      this.speedY = Number((distanceY * speedX) / distanceX);
    }
  }

  isOutsideFrame() {
    if(this.isOnPlace){
      return;
    }

    if(this.x <= 0 && this.y + this.height >= this.frame.height * 0.8){
      let abs = Math.abs(this.speedX);
      this.speedX = abs;
      let abs1 = Math.abs(this.speedY);
      this.speedY = -abs1;
    }else if(this.x <= 0 && this.y <= 0){
      let abs = Math.abs(this.speedX);
      this.speedX = abs;
      let abs1 = Math.abs(this.speedY);
      this.speedY = abs1;
    }else if(this.x + this.width >= this.frame.width && this.y + this.height >= this.frame.height * 0.8){
      let abs = Math.abs(this.speedX);
      this.speedX = -abs;
      let abs1 = Math.abs(this.speedY);
      this.speedY = -abs1;
    }else if(this.x + this.width >= this.frame.width && this.y <= 0){
      let abs = Math.abs(this.speedX);
      this.speedX = -abs;
      let abs1 = Math.abs(this.speedY);
      this.speedY = abs1;
    }else if(this.x <= 0){
      let abs = Math.abs(this.speedX);
      this.speedX = abs;
    }else if(this.x + this.width >= this.frame.width){
      let abs = Math.abs(this.speedX);
      this.speedX = -abs;
    }else if(this.y <= 0){
      let abs = Math.abs(this.speedY);
      this.speedY = abs;
    }else if(this.y + this.height >= this.frame.height * 0.8){
      let abs = Math.abs(this.speedY);
      this.speedY = -abs;
    }
  }

  turnOffRotation() {
    this.rotationOff = true;
  }
  turnOnRotation() {
    this.rotationOff = false;
  }
  isClicked(x,y) {
    if((x >= this.x && x <= (this.x + this.width)) &&
    (y >= this.y && y <= (this.y + this.height))){
      return true;
    }
    return false;
  }
  setPreviousPosition() {
      this.previousX = this.x;
      this.previousY = this.y;
  }

  setSpeed(speedX, speedY) {
    this.speedX = speedX;
    this.speedY = speedY;
  }

  move() {
      let x = 0;
      let y = 0;
      if(typeof this.speedX === 'number'){
        x = this.previousX + this.speedX;
        this.x = x;
      }
      if(typeof this.speedY === 'number'){
        y = this.previousY + this.speedY;
        this.y = y;
      }
  }

  calculateDirection(letter, dot) {
    let x = dot.x;
    let y = dot.y;
    let speedX = this.speedX;
    let speedY = this.speedY;

    let futureX = x + speedX;
    let futureY = y + speedY;
    if(!(futureX >= letter.x && futureX <= letter.x + letter.width &&
    futureY >= letter.y && futureY <= letter.y + letter.height)){
      return;
    }
    futureX = x + (speedX * (-1));
    futureY = y + speedY;

    if(!(futureX >= letter.x && futureX <= letter.x + letter.width &&
    futureY >= letter.y && futureY <= letter.y + letter.height)){
      this.speedX *= -1;
      return;
    }
    futureX = x + speedX
    futureY = y + (speedY * (-1));

    if(!(futureX >= letter.x && futureX <= letter.x + letter.width &&
    futureY >= letter.y && futureY <= letter.y + letter.height)){
      this.speedY *= -1;
      return;
    }

    futureX = x + (speedX * (-1));
    futureY = y + (speedY * (-1));

    if(!(futureX >= letter.x && futureX <= letter.x + letter.width &&
    futureY >= letter.y && futureY <= letter.y + letter.height)){
      this.speedX *= -1;
      this.speedY *= -1;
    }
  }

  touched(letter) {
    if(this.isOnPlace || letter.isOnPlace){
      return;
    }
    if(this.x >= letter.x && this.x <= letter.x + letter.width &&
    this.y >= letter.y && this.y <= letter.y + letter.height){
      this.calculateDirection(letter, {x: this.x, y: this.y});
      return 'lefttop';
    }else if(this.x >= letter.x && this.x <= letter.x + letter.width &&
    this.y + this.height >= letter.y && this.y + this.height <= letter.y + letter.height){
      this.calculateDirection(letter, {x: this.x, y: this.y + this.height});
      return 'leftbottom'
    }else if(this.x + this.width >= letter.x && this.x + this.width <= letter.x + letter.width &&
    this.y <= letter.y + letter.height && this.y >= letter.y){;
      this.calculateDirection(letter, {x: this.x + this.width, y: this.y});
      return 'righttop';
    }else if(this.x + this.width >= letter.x && this.x + this.width <= letter.x + letter.width &&
      this.y + this.height >= letter.y && this.y + this.height <= letter.y + letter.height){
        this.calculateDirection(letter, {x: this.x + this.width, y: this.y + this.height});
        return 'lefttop';
      }
      let side = this.collide(this, letter);
      if(side === 'left'){
          this.calculateDirection(letter, {x: this.x, y: this.y});
      }else if(side === 'right'){
          this.calculateDirection(letter, {x: this.x + this.width, y: this.y});
      }else if(side === 'bottom'){
          this.calculateDirection(letter, {x: this.x, y: this.y + this.height});
      }else if(side === 'top'){
          this.calculateDirection(letter, {x: this.x, y: this.y});;
      }
  }

  collide(r1,r2){
    let dx=(r1.x+r1.width/2)-(r2.x+r2.width/2);
    let dy=(r1.y+r1.height/2)-(r2.y+r2.height/2);
    let width=(r1.width+r2.width)/2;
    let height=(r1.height+r2.height)/2;
    let crossWidth=width*dy;
    let crossHeight=height*dx;
    let collision = 'none';
    if(Math.abs(dx)<=width && Math.abs(dy)<=height){
        if(crossWidth>crossHeight){
            collision = (crossWidth > (-crossHeight)) ? 'bottom' : 'left';
        }else{
            collision = (crossWidth > -(crossHeight)) ? 'right' : 'top';
        }
    }
    return collision;
  }
}
