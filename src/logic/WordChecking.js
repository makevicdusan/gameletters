import {Letter} from './Letter.js';

export class WordChecking {
  constructor() {
    this.places = [];
    this.word = null;
    this.width = 0;
    this.canvas = null;
  }
  setCanvas(canvas) {
    this.canvas = canvas;
  }
  calculateWidth() {
    if(window.innerWidth > 1024){
      return 80;
    }else if(window.innerWidth > 600){
      return (0.00014 * (this.canvas.offsetWidth * this.canvas.offsetHeight));
    }
    return (0.00022 * (this.canvas.offsetWidth * this.canvas.offsetHeight));
  }
  adjustWidth() {
    if(this.word){
      this.width = this.calculateWidth() * this.word.length + (this.calculateWidth() / 8);
    }
  }
  setPlacesPositions() {
    let position = (this.canvas.width - this.width) / 2;
    this.places.forEach((plc) => {
      plc.setPosition(position, this.canvas.height * 0.93);
      plc.setLetterPosition();
      position += this.calculateWidth();
    });
  }
  setWord(word){
    this.word = word;
  }
  pushPlace(place){
    this.places.push(place);
  }
  removeAllPlaces() {
    this.places = [];
  }
  returnLetters(){
    this.places.forEach((p) => {
      p.startReleasing();
    });
  }
  isCorrect(){
    if(!this.places){
      return;
    }
    const places = this.places;
    let corr = true;
    for(let i = 0; i < this.places.length; i++){
      if(this.places[i].letter && this.places[i].letter.letter !== this.word.charAt(i) && places[i].letPosY <= places[i].letter.y + places[i].letter.height
        && places[i].letter.angle === 0){
        corr = false;
        break;
      }
    }
    return corr;
  }
  isFullWord() {
    if(!this.places){
      return;
    }
    let fullLetters = 0;
    const places = this.places;
    for(let i = 0; i < places.length; i++){
      if(places[i].letter instanceof Letter && places[i].letPosY <= places[i].letter.y + places[i].letter.height
        && places[i].letter.angle === 0){
        fullLetters++;
      }
    }
    return fullLetters === this.word.length ? true : false;
  }
  pushInEmpty(letter){
    const places = this.places;
    for(let i = 0; i < places.length; i++){
      if(!places[i].letter){
        places[i].setLetter(letter);
        places[i].startTaking();
        break;
      }
    }
  }

}
