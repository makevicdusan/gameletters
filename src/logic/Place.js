import { generateRandom } from './Utils.js';


export class Place {
  constructor(canvas, width, num) {
    this.num = num;
    this.letter = null;
    this.letPosX = null;
    this.letPosY = null;
    this.x = null;
    this.y = null;
    this.canvas = canvas;
    this.ctx = canvas.getContext('2d');
    this.width = width;
    this.isReturning = false;
    this.isFixed = false;
    this.interval = null;
  }
  setPosition(x, y) {
    this.x = x;
    this.y = y;
  }
  setLetter(letter) {
    this.letter = letter;
    this.letter.isOnPlace = true;
  }
  removeLetter() {
    this.letter.isOnPlace = false;
    this.letter = null;
  }
  calculateDot() {
    if(window.innerWidth > 1024){
      return 5;
    }else if(window.innerWidth > 600){
      return (0.000003 * this.canvas.offsetWidth * this.canvas.offsetHeight);
    }
    return (0.000007 * this.canvas.offsetWidth * this.canvas.offsetHeight);
  }
  setLetterPosition() {
    this.letPosX = this.x + this.calculateDot();
    this.letPosY = this.y - this.calculateDot();
  }

  draw() {
    this.ctx.beginPath();
    this.ctx.moveTo(this.x,this.y);
    this.ctx.lineTo(this.x + this.width, this.y);
    // this.ctx.fillRect(this.letPosX, this.letPosY, 4,4);
    this.ctx.stroke();
  }

  startTaking() {
    const letter = this.letter;
    this.isFixed = false;
    this.isReturning = false;
    letter.setSpeedToDot(null,1.5, this.letPosX, this.letPosY);
    letter.turnOffRotation();
    let distance = letter.calculateDistance(letter.x, letter.y + letter.height, this.letPosX, this.letPosY);
    let speed = letter.calculateSpeed();
    let time = distance / speed;
    let diffAngl;
    if(letter.getAngle() > 0){
      diffAngl = 360 - letter.getAngle();
    }else{
      diffAngl = - 360 - letter.getAngle();
    }
    letter.setRotationSpeed(diffAngl / time);
    this.takeLetter();
  }
  calculateRange() {
    return (0.0000013 * (this.canvas.offsetWidth * this.canvas.offsetHeight));
  }
  takeLetter() {
    this.interval = setInterval(() => {
      if(this.letter.y + this.letter.height >= this.letPosY){
        this.letter.turnOnRotation();
        this.isFixed = true;
        this.letter.speedX = 0;
        this.letter.speedY = 0;
        this.letter.angle = 0;
        this.letter.rotationSpeed = 0;
        clearInterval(this.interval);
      }
    },1);
  }
  startReleasing() {
    this.letter.turnOffRotation();
    this.isReturning = true;
    // let distanceX = this.canvas.width/2 - this.letter.x;
    // let distanceY = this.canvas.height/2 - (this.letter.y + this.letter.height);
      this.letter.setSpeedToDot(null,-1.5, this.canvas.width/2, (this.canvas.height * 0.8) / 2);
      this.releaseLetter();
  }
  releaseLetter() {
    this.interval = setInterval(() => {
      if(this.letter.y <= this.canvas.height/2){
        let range = this.calculateRange();
        let speedX = generateRandom(-range,range, true);
        let speedY = generateRandom(-range,range, true);
        this.letter.setSpeed(speedX, speedY);
        this.letter.setRotationSpeed(generateRandom(-0.8,0.8));
        this.letter.isOnPlace = false;
        this.letter = null;
        clearInterval(this.interval);
      }
    },1);
  }
}
