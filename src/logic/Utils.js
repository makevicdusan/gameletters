export const generateRandom = (min, max, decimal) => {
  if(decimal){
    return Number((Math.random() * (max - min) + min).toFixed(4));
  }
  const min1 = Math.ceil(min);
  const max1 = Math.floor(max);
  return Math.floor(Math.random() * ((max1 - min1) + 1)) + min1;
}
