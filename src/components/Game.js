import React, { Component } from 'react';
import '../css/Game.css';
import { Letter } from '../logic/Letter.js';
import { generateRandom } from '../logic/Utils.js';
import { Place } from '../logic/Place.js';
import { WordChecking } from '../logic/WordChecking.js';
import classNames from 'classnames';

const Status = {
  START: 1,
  RUNNING: 2,
  LOSE: 3,
  WON: 4,
  COMPLETE: 5
}

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: Status.START,
      time: 0
    }
    this.letters = [];
    this.canvas = null;
    this.words = ['PRVAREC', 'DRUGA', 'TRECA'];
    this.wordsIndex = 0;
    this.wordChecking = new WordChecking();
    this.timeInterval = null;
    this.drawInterval = null;
    this.updateInterval = null;
  }

  componentDidMount() {
    this.createCanvas();
  }

  createCanvas() {
    this.canvas = document.getElementById('frame');
    this.canvas.width = this.canvas.offsetWidth;
    this.canvas.height = this.canvas.offsetHeight;
  }

  startTime() {
    this.timeInterval = setInterval(() => {
      this.setState({time: this.state.time + 1});
    }, 1000);
  }
  stopTime() {
    clearInterval(this.timeInterval);
  }
  resetTime(){
    clearInterval(this.timeInterval);
    this.setState({time: 0});
  }
  clearIntervals() {
    clearInterval(this.drawInterval);
    clearInterval(this.updateInterval);
  }
  removeAllLetters() {
    this.letters = [];
  }
  startDrawing() {
    const ctx = this.canvas.getContext('2d');
    this.drawInterval = setInterval(() => {
      // console.log('drawing');
      ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      ctx.beginPath();
      ctx.moveTo(0,this.canvas.height * 0.8);
      ctx.lineTo(this.canvas.width, this.canvas.height * 0.8);
      ctx.stroke();
      this.letters.forEach((l) => {
        l.draw();
      });
      this.wordChecking.places.forEach((p) => {
        p.draw();
      });
    }, 1);
  }
  startLetters() {
    this.letters.forEach((l) => {
      l.update();
    });
  }
  updateGame() {
    this.updateInterval = setInterval(() => {
      if(this.wordChecking.isFullWord()){
        if(this.wordChecking.isCorrect()){
          this.clearIntervals();
          this.stopTime();
          if((this.words.length - 1) > this.wordsIndex){
            this.setState({status: Status.WON})
          }else{
            this.setState({status: Status.COMPLETE})
          }
        }else{
          this.setState({state: Status.LOSE});
          this.wordChecking.returnLetters();
        }
      }
    }, 1);
  }

  handleClick(e) {
    let x = e.pageX - this.canvas.offsetLeft;
    let y = e.pageY - this.canvas.offsetTop;
    this.letters.forEach((l) => {
      let isClicked = l.isClicked(x,y);
      if(isClicked){
        this.wordChecking.pushInEmpty(l);
      }
    });
  }

  handleTouch(e) {
    // let x = e.changedTouches[0].pageX - this.canvas.offsetLeft;
    // let y = e.changedTouches[0].pageY - this.canvas.offsetTop;
    // this.letters.forEach((l) => {
    //   let isClicked = l.isClicked(x,y);
    //   if(isClicked){
    //     this.wordChecking.pushInEmpty(l);
    //   }
    // });
  }

  handleTouchStart(e) {
    if(this.state.status === Status.COMPLETE){
      this.wordsIndex = 0;
      this.resetTime();
      this.removeAllLetters();
      this.wordChecking.removeAllPlaces();
    }
    if(this.state.status === Status.RUNNING){
      return;
    }
    if(this.state.status === Status.WON){
      this.wordsIndex++;
      this.resetTime();
      this.wordChecking.removeAllPlaces();
      this.removeAllLetters();
    }
    this.setState({status: Status.RUNNING});
    this.createLetters();
    this.startDrawing();
    this.updateGame();
    this.setLettersSpeed();
    this.startLetters();
    this.createPlaces();
    this.startTime();
  }

  handleClickStart(e) {
    if(this.state.status === Status.COMPLETE){
      this.wordsIndex = 0;
      this.resetTime();
      this.removeAllLetters();
      this.wordChecking.removeAllPlaces();
    }
    if(this.state.status === Status.RUNNING){
      return;
    }
    if(this.state.status === Status.WON){
      this.wordsIndex++;
      this.resetTime();
      this.wordChecking.removeAllPlaces();
      this.removeAllLetters();
    }
    this.setState({status: Status.RUNNING});
    this.createLetters();
    this.startDrawing();
    this.updateGame();
    this.setLettersSpeed();
    this.startLetters();
    this.createPlaces();
    this.startTime();
  }
  createPlaces() {
    const words = this.words;
    this.wordChecking.word = this.words[this.wordsIndex];
    for(let i = 0; i < words[this.wordsIndex].length; i++){
      this.wordChecking.pushPlace(new Place(this.canvas, this.calculatePlaceAndMiddleSize(), i));
    }
    this.wordChecking.setCanvas(this.canvas);
    this.wordChecking.setWord(words[this.wordsIndex]);
    this.wordChecking.adjustWidth();
    this.wordChecking.setPlacesPositions();
  }
  // calculateWidth() {
  //   if(window.innerWidth > 1024){
  //     return 80;
  //   }else if(window.innerWidth > 600){
  //     return (0.00014 * (this.canvas.offsetWidth * this.canvas.offsetHeight));
  //   }
  //   return (0.00026 * (this.canvas.offsetWidth * this.canvas.offsetHeight));
  // }
  calculatePlaceAndMiddleSize() {
    if(window.innerWidth > 1024){
      return 70;
    }else if(window.innerWidth > 600){
      return (0.00011 * (this.canvas.offsetWidth * this.canvas.offsetHeight));
    }
    return (0.0002 * (this.canvas.offsetWidth * this.canvas.offsetHeight));
  }
  calculateLetterSize() {
    if(window.innerWidth > 1024){
      return 60;
    }else if(window.innerWidth > 600){
      return 0.00009 * (this.canvas.offsetWidth * this.canvas.offsetHeight);
    }
    return (0.00015 * (this.canvas.offsetWidth * this.canvas.offsetHeight));
  }
  calculateMobileInfoClass() {
    if(this.state.status === Status.RUNNING || this.state.status === Status.LOSE){
      return false;
    }
    return true;
  }
  calculateRange() {
    return (0.0000013 * (this.canvas.offsetWidth * this.canvas.offsetHeight));
  }
  createLetters() {
    const words = this.words;
    for(let i = 0; i < words[this.wordsIndex].length; i++){
      let word = words[this.wordsIndex];
      let letterSize = this.calculateLetterSize();
      let x = generateRandom(0, this.canvas.width - letterSize);
      let y = generateRandom(letterSize, this.canvas.height * 0.8 - letterSize);
      let letter = new Letter(this.canvas,x,y,letterSize,letterSize,word.charAt(i));
      letter.letters = this.letters;
      this.letters.push(letter);
    }
  }

  setLettersSpeed() {
    this.letters.forEach((letter) => {
      let rotationSpeed = generateRandom(-0.8,0.8,true);
      letter.setRotationSpeed(rotationSpeed);
      let range = this.calculateRange();
      let speedX = generateRandom(-range,range, true);
      let speedY = generateRandom(-range,range, true);
      letter.setSpeed(speedX,speedY);
    });
  }

  renderButtonText() {
    let text;
  }
  renderCurrentWord() {
    return (<div className='current-word'>{this.words[this.wordsIndex]}</div>);
  }
  renderGameStatus() {
    let message;
    let buttonText;
    if(this.state.status === Status.START) {
      message = 'Start your game';
      buttonText = 'Start';
    }else if(this.state.status === Status.WON){
      message = 'Start new level';
      buttonText = 'New level';
    }else if(this.state.status === Status.COMPLETE){
      message = 'Victory, play again';
      buttonText = 'Play again'
    }else if(this.state.status === Status.LOSE){
      message = 'Wrong word';
    }else{
      message = 'Game is running'
      buttonText = 'Start';
    }
    if(window.innerWidth < 768){
      return (
        <div className='message-button-wrap'>
          <div className='message-wrap'>{message}</div>
        <button className='start-button' onClick={(e) => {
          this.handleClickStart();
        }}
        onTouchEnd={(e) => {
          this.handleTouchStart(e);
        }}>{buttonText}</button>
        </div>
      );
    }
    return (
      <div className='message-button-wrap'>
      <button className='start-button' onClick={(e) => {
        this.handleClickStart();
      }}
      onTouchEnd={(e) => {
        this.handleTouchStart(e);
      }}>{buttonText}</button>
    <div className='message-wrap'>{message}</div>
      </div>
    );
  }
  render() {
    return (
      <div className='wrap'>
        {this.renderCurrentWord()}
        <div>
          <canvas onClick={(e) => {
              this.handleClick(e);
            }} id='frame'>
          </canvas>
        </div>
        <div className={classNames({
              'info-wrap': true,
              'info-wrap-mobile': true,
              'info-wrap-mobile-down': !this.calculateMobileInfoClass(),
              'info-wrap-mobile-up': this.calculateMobileInfoClass()
          })}>
          <div className='time-wrap'>
            Time: { this.state.time }
          </div>

        {this.renderGameStatus()}
        </div>
      </div>
    );
  }
}

export default Game;
