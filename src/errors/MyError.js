export class MyError {
    constructor(className, funcName, name, message) {
        this.className = className;
        this.funcName = funcName;
        this.name = name;
        this.message = message;
    }
}
